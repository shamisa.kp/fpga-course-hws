library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity SynchronousReset_tb is
end;

architecture bench of SynchronousReset_tb is

  component SynchronousReset
      generic (P: integer:= 5;
  			   N: unsigned:= "10000");
      Port (clk: in std_logic;
            reset: in std_logic;
            output_clk: out std_logic);
  end component;

  signal clk: std_logic;
  signal reset: std_logic;
  signal output_clk: std_logic;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: SynchronousReset generic map ( P          => 5,
                                      N          => "10000")
                           port map ( clk        => clk,
                                      reset      => reset,
                                      output_clk => output_clk);

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset <= '1';
    wait for clock_period;
    reset <= '0';
    wait for 64 * clock_period;
    


    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
  