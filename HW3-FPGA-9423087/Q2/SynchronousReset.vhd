	library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity SynchronousReset is

    generic (P: integer:= 4;
			 N: unsigned:= "0011");
    Port (clk: in std_logic;
          reset: in std_logic;
          output_clk: out std_logic;
          h_o: out std_logic_vector(P-1 downto 0));
    
end SynchronousReset;

architecture SR of SynchronousReset is
signal sr_clk_count: unsigned(P-1 downto 0);
signal sr_clk_divider: unsigned(P-1 downto 0);
signal sr_clk_divider_h: unsigned(P-1 downto 0);
signal sr_clk_tempCount: unsigned(P-1 downto 0);
begin

sr_clk_divider <= N - 1;
sr_clk_divider_h <= N / 2;
process(clk, reset)
    begin
        if(clk'event and clk = '1') then
			if(reset = '1') then
				 sr_clk_count <= (others => '0');
                 output_clk <= '0';  
            elsif(sr_clk_count < sr_clk_divider_h) then	
                sr_clk_count <= sr_clk_tempCount + 1;
                output_clk <= '0';
            elsif(sr_clk_count = sr_clk_divider) then
                sr_clk_count <= (others => '0');
                output_clk <= '1';
            else
                sr_clk_count <= sr_clk_tempCount + 1;
                output_clk <= '1';
            end if;
         end if;
end process;
sr_clk_tempCount <= sr_clk_count;

end SR;
