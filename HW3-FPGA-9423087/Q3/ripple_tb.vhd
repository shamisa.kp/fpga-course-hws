library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity ripple_tb is
end;

architecture bench of ripple_tb is

  component nBitRipCount
    generic (
      N : integer := 8
    );
    port (
      clk   : in std_logic;
      reset : in std_logic;
      r_out  : out std_logic_vector(N-1 downto 0)
    );
  end component;

  signal clk: std_logic;
  signal reset: std_logic;
  signal r_out: std_logic_vector(7 downto 0);
  
  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;


begin
  uut: nBitRipCount generic map ( N       =>  8)
                        port map ( clk     => clk,
                                   reset   => reset,
                                   r_out => r_out);

 stimulus: process
  begin
   reset <= '1';
   wait for clock_period;
   reset <= '0';
   wait for 36 * clock_period;
   stop_the_clock <= true;
   wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;



end;