library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity nBitRipCount is

    generic( N: natural :=4);
    Port (clk: in std_logic;
          reset: in std_logic;
          r_out: out std_logic_vector(N-1 downto 0));
          
end nBitRipCount;

architecture ripple_counter of nBitRipCount is

signal clk_temp: std_logic_vector(N-1 downto 0);
signal o_temp: std_logic_vector(N-1 downto 0);

begin
clk_temp(0) <= clk;
clk_temp(N-1 downto 1) <= o_temp(N-2 downto 0);

genRipCount: for i in  0 to N - 1 generate
    dff: process(reset, clk_temp)
    begin 
        if(reset = '1') then
         o_temp(i) <= '1';
        elsif(clk_temp(i)'event and clk_temp(i) = '1') then
            o_temp(i) <= not o_temp(i);
        end if;
    end process;
end generate;

r_out <= not o_temp;

end ripple_counter;
