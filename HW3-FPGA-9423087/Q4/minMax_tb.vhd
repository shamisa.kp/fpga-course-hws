library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity min_max_data_tb is
end;

architecture bench of min_max_data_tb is

  component min_max_data
      Port (data: in std_logic_vector(7 downto 0);
            start_bit: in std_logic;
            clk: in std_logic;
            reset: in std_logic;
            w: out std_logic_vector(7 downto 0);
            done_bit: out std_logic);
  end component;

  signal data: std_logic_vector(7 downto 0);
  signal start_bit: std_logic;
  signal clk: std_logic;
  signal reset: std_logic;
  signal w: std_logic_vector(7 downto 0);
  signal done_bit: std_logic;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: min_max_data port map ( data      => data,
                               start_bit => start_bit,
                               clk       => clk,
                               reset     => reset,
                               w         => w,
                               done_bit  => done_bit );

  stimulus: process
  begin
  
    -- Put initialisation code here

    reset <= '1';
    start_bit <= '0';
    data <= "00000000";
    wait for clock_period;
    reset <= '0';
    wait for clock_period;
    start_bit <= '1';
    wait for clock_period;
    start_bit <= '0';
    
    data <= "00000111";
    wait for clock_period;
    data <= "00000110";
    wait for clock_period;
    data <= "00110000";
    wait for clock_period;
    data <= "00010100";
    wait for clock_period;
    data <= "01001010";
    wait for clock_period;
    data <= "00000111";
    wait for clock_period;
    data <= "11100001";
    wait for clock_period;
    data <= "00100000";
    wait for clock_period;
    data <= "01010100";
    wait for clock_period;
    data <= "00001000";
    wait for clock_period * 8;


    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
  