----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/25/2018 10:56:41 PM
-- Design Name: 
-- Module Name: min_max_data - 8DataMinMax
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity min_max_data is

    Port (data: in std_logic_vector(7 downto 0);
          start_bit: in std_logic;
          clk: in std_logic;
          reset: in std_logic;
          w: out std_logic_vector(7 downto 0);
          done_bit: out std_logic);
          
end min_max_data;

architecture DataMinMax of min_max_data is
 
--type data_input is array (9 downto 0) of unsigned(7 downto 0);
--signal d_in: data_input;
signal min_data: unsigned(7 downto 0);
signal max_data: unsigned(7 downto 0);
signal counterTemp: unsigned(3 downto 0);
signal counterNext: unsigned(3 downto 0);
signal saveState: unsigned(1 downto 0);

begin
process(reset, clk, start_bit)
    begin
        if(reset = '1') then
            counterTemp <= "0000";
            done_bit <= '0';
            w <= (others => '0');
            min_data <= (others => '1');
            max_data <= (others => '0');
            saveState <= "00";
        elsif(start_bit = '1' and saveState = "00") then
            saveState <= "01";
        elsif(start_bit <= '0' and saveState = "01") then
            saveState <= "10";
        elsif(clk'event and clk = '1' and saveState = "10") then
            if(counterTemp /= "1010") then
                if(unsigned(data) > max_data) then
                    max_data <= unsigned(data);
                elsif(unsigned(data) < min_data) then
                    min_data <= unsigned(data);
                end if;
                counterTemp <= counterNext + 1;
            elsif(counterTemp = "1010") then
                w <= std_logic_vector((min_data + max_data) / 2);
                done_bit <= '1';
                saveState <= "11";
            end if;   
        elsif(clk'event and clk = '1' and saveState = "11") then
            done_bit <= '0';
            saveState <= "00";
        end if;
 end process;
 
counterNext <= counterTemp;
                   
end DataMinMax;
