library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity division_tb is
end division_tb;

architecture arch of division_tb is

  component Division
    port (clk, reset, d_in: in std_logic;
            d_out: out std_logic_vector(7 downto 0);
            cnt: out std_logic_vector(3 downto 0);
             data: out std_logic_vector(7 downto 0));
  end component;

  signal clk, reset, d_in: std_logic;
  signal d_out, data: std_logic_vector(7 downto 0);
  signal cnt: std_logic_vector(3 downto 0);

  constant clock_period: time := 10 ns;
  signal stop: boolean;

begin

  uut: Division port map ( clk => clk,
                      reset => reset,
                      d_in => d_in,
                      d_out => d_out,
                      cnt => cnt,
                      data => data);

  stimulus: process
  begin
  
    reset <= '1';
    wait for clock_period;
    reset <= '0';
    
    d_in <= '0';
    wait for clock_period * 2;
    
    d_in <= '1';
    wait for clock_period;
    
    --
    d_in <= '1';
    wait for clock_period;
    
    d_in <= '1';
    wait for clock_period;
    
    d_in <= '0';
    wait for clock_period;
    
    d_in <= '1';
    wait for clock_period;
                    
    d_in <= '0';
    wait for clock_period;
    
    d_in <= '1';
    wait for clock_period;
    
    d_in <= '0';
    wait for clock_period;
    
    d_in <= '1';
    wait for clock_period;
    --
    d_in <= '0';
    wait for clock_period * 2;
   
    stop <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;