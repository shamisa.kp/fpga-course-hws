library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity Division is
    Port (clk, reset, d_in: in std_logic;
          d_out: out std_logic_vector(7 downto 0);
          cnt: out std_logic_vector(3 downto 0);
          data: out std_logic_vector(7 downto 0));
end Division;

architecture Behavioral of Division is


type my_state is(idle, start, data_read, done);
signal state_reg, state_next: my_state;
signal data_temp, data_temp_next: unsigned(7 downto 0);
signal data_length, data_length_next: unsigned(3 downto 0);
signal b: unsigned(1 downto 0);

begin
b <= "11";
process(clk, reset)
    begin
        if(reset = '1') then
            state_reg <= idle;
            data_temp <= (others => '0');
            data_length <= (others => '0');
        elsif(clk'event and clk = '1') then
            state_reg <= state_next;
            data_temp <= data_temp_next;
            data_length <= data_length_next;
       end if;
     end process;
     
  
process(state_reg, d_in, data_length)
    begin 
    state_next <= state_reg;
    data_length_next <= data_length;
    data_temp_next <= data_temp;
    
        case state_reg is
        
            when idle =>
                if(d_in = '1') then
                    data_length_next <= (others => '0');
                    state_next <= start;
                end if;
                
            when start =>
                    state_next <= data_read;
                    data_temp_next <= data_temp(6 downto 0) & d_in;
                    
            when data_read =>
                    if(data_length /= "0111") then
                        data_length_next <= data_length + 1;
                        data_temp_next <= data_temp(6 downto 0) & d_in;
                    else 
                        state_next <= done;
                    end if;
                    
            when done =>
                state_next <= idle;
        end case;
    end process;
    d_out <= std_logic_vector(data_temp);
    cnt <= std_logic_vector(data_length);
    data <= std_logic_vector(data_temp);
            
end Behavioral;
