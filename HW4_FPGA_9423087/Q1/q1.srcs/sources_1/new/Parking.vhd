library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;


entity Parking is
    Port (clk, reset, enter_gate, exit_gate: in std_logic;
          is_full: out std_logic;
          p_counter: out std_logic_vector(3 downto 0));
end Parking;

architecture arch of Parking is
signal capacity_reg, capacity_next: unsigned(3 downto 0);
signal full_reg: std_logic;
signal enter_reg, exit_reg: std_logic;

begin
 process(clk, reset)
   begin
       if(reset = '1') then
           enter_reg <= '0';
           full_reg <= '0';
           exit_reg <= '0';
           capacity_next <= "0101";
       elsif(clk'event and clk = '1') then
           enter_reg <= enter_gate;
           exit_reg <= exit_gate;
           capacity_reg <= capacity_next;
           if(enter_gate = '1' and enter_reg = '0') then
            if(capacity_reg /= "1010") then
                capacity_next <= capacity_reg + 1;
            end if;
           elsif(exit_gate = '1' and exit_reg = '0') then
                if(capacity_reg /= "0000") then
                    capacity_next <= capacity_reg - 1;
                end if;
            end if;
       end if;
   end process;
    
   p_counter <= std_logic_vector(capacity_reg);
   is_full <= '1' when capacity_reg = "1010" else '0';
   
end arch;
