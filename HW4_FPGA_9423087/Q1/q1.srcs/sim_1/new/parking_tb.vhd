library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity parking_tb is
end parking_tb;

architecture Behavioral of parking_tb is

    component Parking
        port(clk, reset, enter_gate, exit_gate: in std_logic;
             is_full: out std_logic;
             p_counter: out std_logic_vector(3 downto 0));
    end component;
    
    signal clk, reset, enter_gate, exit_gate: std_logic;
    signal is_full: std_logic;
    signal p_counter: std_logic_vector(3 downto 0);
    
     signal stop_clk: boolean;  
    constant period_clk: time := 20 ns;
    
begin
      parkmap: Parking port map ( clk => clk,
                    reset => reset,
                    enter_gate => enter_gate,
                    exit_gate  => exit_gate,
                    is_full => is_full,
                    p_counter => p_counter);

stimulus: process
begin

  reset <= '1';
  wait for 2 * period_clk;
  reset <= '0';

  enter_gate <= '0';
  exit_gate <= '0';
  wait for period_clk;
  
  enter_gate <= '1';
  wait for period_clk;
  
  enter_gate <= '0';
  wait for period_clk;
  
  enter_gate <= '1';
  wait for period_clk;
  
  enter_gate <= '0';
  wait for period_clk;
  
  enter_gate <= '1';
  wait for period_clk;
  
  enter_gate <= '0';
  wait for period_clk;
  
  enter_gate <= '1';
  wait for period_clk;
  
  enter_gate <= '0';
  wait for period_clk;
  
  enter_gate <= '1';
  wait for period_clk;
  
  exit_gate <= '0';
  wait for period_clk;
  
  exit_gate <= '1';
  wait for period_clk;
  
  enter_gate <= '0';
  wait for period_clk;
  
  enter_gate <= '1';
  wait for period_clk;
  
  enter_gate <= '0';
  wait for period_clk;
  
  exit_gate <= '0';
  wait for period_clk;
  
  exit_gate <= '1';
  wait for period_clk;
  exit_gate <= '0';
  wait for period_clk;
  exit_gate <= '1';
  wait for period_clk;
  exit_gate <= '0';
  wait for period_clk;
  exit_gate <= '1';
  wait for period_clk;
  exit_gate <= '0';
  wait for period_clk;
  exit_gate <= '1';
  wait for period_clk;
  exit_gate <= '0';
  wait for period_clk;
  exit_gate <= '1';
  wait for period_clk;
  exit_gate <= '0';
  wait for period_clk;
  exit_gate <= '1';
  wait for period_clk;
  exit_gate <= '0';
  wait for period_clk;
  exit_gate <= '1';
  wait for period_clk;
  exit_gate <= '0';
  wait for period_clk;
  exit_gate <= '1';
  wait for period_clk;
  exit_gate <= '0';
  wait for period_clk;
  exit_gate <= '1';
  wait for period_clk;
  exit_gate <= '0';
  wait for period_clk;
  exit_gate <= '1';
  wait for period_clk;
  
  stop_clk <= true;
  wait;
end process;


clock_adjusment: process
    begin
        while not stop_clk loop
            clk <= '0', '1' after period_clk / 2;
            wait for period_clk;
        end loop;
        wait;
   end process;


end Behavioral;
