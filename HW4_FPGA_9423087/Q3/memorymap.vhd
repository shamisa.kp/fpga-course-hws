library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity eq3 is
  port (
    clk   : in std_logic;
    reset : in std_logic;
    counterA : out std_logic_vector(8 downto 0);
    counterB : out std_logic_vector(8 downto 0);
    counterC : out std_logic_vector(8 downto 0);
    counterD : out std_logic_vector(8 downto 0);
    counterO : out std_logic_vector(7 downto 0);
    dataO: out std_logic_vector(7 downto 0)
  );
end eq3;

architecture arch of eq3 is
    signal counter_reg, counter_next : unsigned(7 downto 0);
    signal f : std_logic := '0';
    signal data : std_logic_vector(7 downto 0);
    signal counterA_reg, counterA_next : unsigned(8 downto 0);
    signal counterB_reg, counterB_next : unsigned(8 downto 0);
    signal counterC_reg, counterC_next : unsigned(8 downto 0);
    signal counterD_reg, counterD_next : unsigned(8 downto 0);
    type rom_type is array(0 to 255) of std_logic_vector(7 downto 0);
    constant MyROMTable : rom_type := (
        "01000010",   -- addr 0
        "01000011",   -- addr 1
        "01000001",   -- addr 2
        "01000010",   -- addr 3
        "01000001",   -- addr 4
        "01000001",   -- addr 5
        "01000001",   -- addr 6
        "01000010",   -- addr 7
        "01000010",   -- addr 8
        "01000011",   -- addr 9
        "01000010",   -- addr 10
        "01000011",   -- addr 11
        "01000001",   -- addr 12
        "01000100",   -- addr 13
        "01000001",   -- addr 14
        "01000011",   -- addr 15
        "01000010",   -- addr 16
        "01000011",   -- addr 17
        "01000001",   -- addr 18
        "01000001",   -- addr 19
        "01000100",   -- addr 20
        "01000100",   -- addr 21
        "01000010",   -- addr 22
        "01000011",   -- addr 23
        "01000100",   -- addr 24
        "01000100",   -- addr 25
        "01000001",   -- addr 26
        "01000001",   -- addr 27
        "01000001",   -- addr 28
        "01000100",   -- addr 29
        "01000001",   -- addr 30
        "01000010",   -- addr 31
        "01000100",   -- addr 32
        "01000011",   -- addr 33
        "01000011",   -- addr 34
        "01000010",   -- addr 35
        "01000011",   -- addr 36
        "01000100",   -- addr 37
        "01000001",   -- addr 38
        "01000100",   -- addr 39
        "01000100",   -- addr 40
        "01000011",   -- addr 41
        "01000010",   -- addr 42
        "01000100",   -- addr 43
        "01000001",   -- addr 44
        "01000010",   -- addr 45
        "01000100",   -- addr 46
        "01000010",   -- addr 47
        "01000010",   -- addr 48
        "01000001",   -- addr 49
        "01000001",   -- addr 50
        "01000011",   -- addr 51
        "01000001",   -- addr 52
        "01000010",   -- addr 53
        "01000010",   -- addr 54
        "01000001",   -- addr 55
        "01000011",   -- addr 56
        "01000001",   -- addr 57
        "01000011",   -- addr 58
        "01000011",   -- addr 59
        "01000001",   -- addr 60
        "01000010",   -- addr 61
        "01000011",   -- addr 62
        "01000010",   -- addr 63
        "01000001",   -- addr 64
        "01000011",   -- addr 65
        "01000011",   -- addr 66
        "01000011",   -- addr 67
        "01000100",   -- addr 68
        "01000011",   -- addr 69
        "01000100",   -- addr 70
        "01000001",   -- addr 71
        "01000001",   -- addr 72
        "01000100",   -- addr 73
        "01000010",   -- addr 74
        "01000001",   -- addr 75
        "01000100",   -- addr 76
        "01000010",   -- addr 77
        "01000100",   -- addr 78
        "01000011",   -- addr 79
        "01000100",   -- addr 80
        "01000011",   -- addr 81
        "01000100",   -- addr 82
        "01000010",   -- addr 83
        "01000010",   -- addr 84
        "01000100",   -- addr 85
        "01000010",   -- addr 86
        "01000100",   -- addr 87
        "01000011",   -- addr 88
        "01000011",   -- addr 89
        "01000001",   -- addr 90
        "01000100",   -- addr 91
        "01000010",   -- addr 92
        "01000011",   -- addr 93
        "01000010",   -- addr 94
        "01000001",   -- addr 95
        "01000100",   -- addr 96
        "01000011",   -- addr 97
        "01000001",   -- addr 98
        "01000011",   -- addr 99
        "01000010",   -- addr 100
        "01000011",   -- addr 101
        "01000100",   -- addr 102
        "01000010",   -- addr 103
        "01000100",   -- addr 104
        "01000011",   -- addr 105
        "01000001",   -- addr 106
        "01000100",   -- addr 107
        "01000011",   -- addr 108
        "01000100",   -- addr 109
        "01000001",   -- addr 110
        "01000001",   -- addr 111
        "01000100",   -- addr 112
        "01000011",   -- addr 113
        "01000001",   -- addr 114
        "01000100",   -- addr 115
        "01000100",   -- addr 116
        "01000100",   -- addr 117
        "01000011",   -- addr 118
        "01000001",   -- addr 119
        "01000001",   -- addr 120
        "01000001",   -- addr 121
        "01000001",   -- addr 122
        "01000001",   -- addr 123
        "01000100",   -- addr 124
        "01000011",   -- addr 125
        "01000011",   -- addr 126
        "01000100",   -- addr 127
        "01000001",   -- addr 128
        "01000010",   -- addr 129
        "01000011",   -- addr 130
        "01000100",   -- addr 131
        "01000011",   -- addr 132
        "01000001",   -- addr 133
        "01000100",   -- addr 134
        "01000001",   -- addr 135
        "01000100",   -- addr 136
        "01000010",   -- addr 137
        "01000100",   -- addr 138
        "01000011",   -- addr 139
        "01000011",   -- addr 140
        "01000001",   -- addr 141
        "01000001",   -- addr 142
        "01000001",   -- addr 143
        "01000001",   -- addr 144
        "01000001",   -- addr 145
        "01000001",   -- addr 146
        "01000011",   -- addr 147
        "01000011",   -- addr 148
        "01000001",   -- addr 149
        "01000001",   -- addr 150
        "01000100",   -- addr 151
        "01000011",   -- addr 152
        "01000001",   -- addr 153
        "01000010",   -- addr 154
        "01000011",   -- addr 155
        "01000001",   -- addr 156
        "01000011",   -- addr 157
        "01000100",   -- addr 158
        "01000100",   -- addr 159
        "01000001",   -- addr 160
        "01000010",   -- addr 161
        "01000011",   -- addr 162
        "01000100",   -- addr 163
        "01000001",   -- addr 164
        "01000001",   -- addr 165
        "01000001",   -- addr 166
        "01000010",   -- addr 167
        "01000011",   -- addr 168
        "01000011",   -- addr 169
        "01000010",   -- addr 170
        "01000100",   -- addr 171
        "01000011",   -- addr 172
        "01000010",   -- addr 173
        "01000011",   -- addr 174
        "01000011",   -- addr 175
        "01000011",   -- addr 176
        "01000010",   -- addr 177
        "01000001",   -- addr 178
        "01000010",   -- addr 179
        "01000011",   -- addr 180
        "01000001",   -- addr 181
        "01000100",   -- addr 182
        "01000001",   -- addr 183
        "01000010",   -- addr 184
        "01000100",   -- addr 185
        "01000001",   -- addr 186
        "01000011",   -- addr 187
        "01000011",   -- addr 188
        "01000100",   -- addr 189
        "01000010",   -- addr 190
        "01000001",   -- addr 191
        "01000011",   -- addr 192
        "01000100",   -- addr 193
        "01000100",   -- addr 194
        "01000100",   -- addr 195
        "01000001",   -- addr 196
        "01000001",   -- addr 197
        "01000011",   -- addr 198
        "01000100",   -- addr 199
        "01000100",   -- addr 200
        "01000011",   -- addr 201
        "01000100",   -- addr 202
        "01000011",   -- addr 203
        "01000010",   -- addr 204
        "01000010",   -- addr 205
        "01000011",   -- addr 206
        "01000011",   -- addr 207
        "01000100",   -- addr 208
        "01000100",   -- addr 209
        "01000010",   -- addr 210
        "01000100",   -- addr 211
        "01000001",   -- addr 212
        "01000001",   -- addr 213
        "01000001",   -- addr 214
        "01000011",   -- addr 215
        "01000001",   -- addr 216
        "01000100",   -- addr 217
        "01000100",   -- addr 218
        "01000001",   -- addr 219
        "01000001",   -- addr 220
        "01000010",   -- addr 221
        "01000001",   -- addr 222
        "01000100",   -- addr 223
        "01000010",   -- addr 224
        "01000100",   -- addr 225
        "01000011",   -- addr 226
        "01000100",   -- addr 227
        "01000100",   -- addr 228
        "01000100",   -- addr 229
        "01000010",   -- addr 230
        "01000011",   -- addr 231
        "01000100",   -- addr 232
        "01000010",   -- addr 233
        "01000010",   -- addr 234
        "01000011",   -- addr 235
        "01000001",   -- addr 236
        "01000011",   -- addr 237
        "01000010",   -- addr 238
        "01000100",   -- addr 239
        "01000010",   -- addr 240
        "01000100",   -- addr 241
        "01000011",   -- addr 242
        "01000001",   -- addr 243
        "01000100",   -- addr 244
        "01000011",   -- addr 245
        "01000001",   -- addr 246
        "01000010",   -- addr 247
        "01000011",   -- addr 248
        "01000100",   -- addr 249
        "01000001",   -- addr 250
        "01000100",   -- addr 251
        "01000010",   -- addr 252
        "01000100",   -- addr 253
        "01000011",   -- addr 254
        "01000100"   -- addr 255
    );
    
begin
    process(reset, clk)
    begin
        if(reset = '1') then
            counter_reg <= (others => '0');
            counterA_reg <= (others => '0');
            counterB_reg <= (others => '0');
            counterC_reg <= (others => '0');
            counterD_reg <= (others => '0');
            
        elsif(clk'event and clk = '1') then
            if(f = '0') then
                data <= MyRomTable(to_integer(counter_reg));
                counter_reg <= counter_next;
                counterA_reg <= counterA_next; 
                counterB_reg <= counterB_next;  
                counterC_reg <= counterC_next;  
                counterD_reg <= counterD_next;
                if(counter_reg = "11111111") then
                    f <= '1';
                end if; 
            end if;                      
        end if;
    end process;
    
    counterA_next <= counterA_reg + 1 when data = "01000001" else counterA_reg;
    counterB_next <= counterB_reg + 1 when data = "01000010" else counterB_reg;
    counterC_next <= counterC_reg + 1 when data = "01000011" else counterC_reg;
    counterD_next <= counterD_reg + 1 when data = "01000100" else counterD_reg;
    counter_next <= counter_reg + 1;
    
    counterA <= std_logic_vector(counterA_reg);
    counterB <= std_logic_vector(counterB_reg);
    counterC <= std_logic_vector(counterC_reg);
    counterD <= std_logic_vector(counterD_reg);
    counterO <= std_logic_vector(counter_reg);
    dataO <= data;
end arch;