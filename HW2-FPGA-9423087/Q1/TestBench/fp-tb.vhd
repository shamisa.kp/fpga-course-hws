library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity fixedPoint_tb is
end;

architecture bench of fixedPoint_tb is

  component fixedPoint
  Port (Num1, Num2 : in std_logic_vector(4 downto 0);
        P1, P2 : in std_logic_vector(1 downto 0);
        Num3 : out std_logic_vector(8 downto 0);
        P3 : out std_logic_vector(2 downto 0));
  end component;

  signal Num1, Num2: std_logic_vector(4 downto 0);
  signal P1, P2: std_logic_vector(1 downto 0);
  signal Num3: std_logic_vector(8 downto 0);
  signal P3: std_logic_vector(2 downto 0);

begin

  uut: fixedPoint port map ( Num1 => Num1,
                             Num2 => Num2,
                             P1   => P1,
                             P2   => P2,
                             Num3 => Num3,
                             P3   => P3 );

  stimulus: process
  begin

    -- Put initialisation code here
    Num1 <= "00010";
    Num2 <= "00101";
    P1 <= "01";
    p2 <= "10";
    
    wait for 100 ns;
    
    Num1 <= "10010";
    Num2 <= "01101";
    P1 <= "11";
    p2 <= "11";
    
    wait for 100 ns;
    
    Num1 <= "11111";
    Num2 <= "11111";
    P1 <= "00";
    p2 <= "10";
    
    wait for 100 ns;
    
    Num1 <= "00010";
    Num2 <= "11001";
    P1 <= "10";
    p2 <= "01";
    
    wait for 100 ns;
    
    Num1 <= "11010";
    Num2 <= "00011";
    P1 <= "01";
    p2 <= "11";
    
    wait for 100 ns;
    
    Num1 <= "10100";
    Num2 <= "00011";
    P1 <= "00";
    p2 <= "10";
    
    wait for 100 ns;
    
    
    

    -- Put test bench stimulus code here

    wait;
  end process;


end;