library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fixedPoint is
Port (Num1, Num2 : in std_logic_vector(4 downto 0);
      P1, P2 : in std_logic_vector(1 downto 0);
      Num3 : out std_logic_vector(8 downto 0);
      P3 : out std_logic_vector(2 downto 0));
      
end fixedPoint;

architecture Behavioral of fixedPoint is
-- using component of multiply 
    component multiply is
        port(a, b: in std_logic_vector(3 downto 0);
             result : out std_logic_vector(7 downto 0));
     end component;
     
     component byteAddorSub is 
             port(a : in std_logic_vector(7 downto 0);
                  b : in std_logic_vector(7 downto 0);
                  addorsub: in std_logic;
                  cin : in std_logic;
                  s : out std_logic_vector(7 downto 0);
                  co : out std_logic);
         end component;

-- signals for signed bit 
    signal cout: std_logic;
    signal tempa, tempb: std_logic_vector(7 downto 0);
    signal tempP: std_logic_vector(7 downto 0);
    signal tempS: std_logic_vector(4 downto 0);
    

begin
    Num3(8) <= Num1(4) xor Num2(4);
    tempa <= "000000" & P1(1 downto 0);
    tempb <= "000000" & P2(1 downto 0);
    
--port map
    point: byteAddorSub port map(a => tempa, b => tempb, addorsub => '0', cin => '0', co => cout,
                          s(7 downto 3) => tempS, s(2 downto 0) => P3);
    answer: multiply port map(a =>  Num1(3 downto 0), b => Num2(3 downto 0), result => Num3(7 downto 0));
     
end Behavioral;
