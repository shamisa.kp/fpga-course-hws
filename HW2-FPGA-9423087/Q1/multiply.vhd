library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- inputs and output
entity multiply is
    port(a, b: in std_logic_vector(3 downto 0);
         result: out std_logic_vector(7 downto 0));
end multiply;

architecture Behavioral of multiply is
-- using component of byteAddorSub for multiplying
    component byteAddorSub is 
        port(a : in std_logic_vector(7 downto 0);
             b : in std_logic_vector(7 downto 0);
             addorsub: in std_logic;
             cin : in std_logic;
             s : out std_logic_vector(7 downto 0);
             co : out std_logic);
    end component;

-- signals for carries and total summation
    signal c3, c2, c1, c0, r_1, r_2: std_logic_vector(7 downto 0);
    signal c_o2, c_o1, c_o0: std_logic;
    

begin
    c0(0) <= a(0) and b(0);
    c0(1) <= a(1) and b(0);
    c0(2) <= a(2) and b(0);
    c0(3) <= a(3) and b(0);
    c0(7 downto 4) <= "0000";
    
    c1(0) <= '0';
    c1(1) <= a(0) and b(1);
    c1(2) <= a(1) and b(1);
    c1(3) <= a(2) and b(1);
    c1(4) <= a(3) and b(1);
    c1(7 downto 5) <= "000";
    
    c2(1 downto 0) <= "00";
    c2(2) <= a(0) and b(2);
    c2(3) <= a(1) and b(2);
    c2(4) <= a(2) and b(2);
    c2(5) <= a(3) and b(2);
    c2(7 downto 6) <= "00";
    
    
    c3(2 downto 0) <= "000";
    c3(3) <= a(0) and b(3);
    c3(4) <= a(1) and b(3);
    c3(5) <= a(2) and b(3);
    c3(6) <= a(3) and b(3);
    c3(7) <= '0';

-- port mapping
    firstAdd: byteAddorSub port map(a => c0, b => c1, addorsub => '0', cin => '0', co => c_o0, s => r_1);
    secondAdd: byteAddorSub port map(a => c2, b => c3, addorsub => '0', cin => c_o0 , co => c_o1, s => r_2);
    finalAnswer: byteAddorSub port map(a => r_1, b => r_2, addorsub => '0', cin => c_o1 , co => c_o2, s => result); 
    

end Behavioral;
