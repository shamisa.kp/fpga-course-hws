library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity byteAddorSub_tb is
end;

architecture bench of byteAddorSub_tb is

  component byteAddorSub
      port(a : in std_logic_vector(7 downto 0);
           b : in std_logic_vector(7 downto 0);
           addorsub: in std_logic;
           cin : in std_logic;
           s : out std_logic_vector(7 downto 0);
           co : out std_logic);
  end component;

  signal a: std_logic_vector(7 downto 0);
  signal b: std_logic_vector(7 downto 0);
  signal addorsub: std_logic;
  signal cin: std_logic;
  signal s: std_logic_vector(7 downto 0);
  signal co: std_logic;

begin

  uut: byteAddorSub port map ( a        => a,
                               b        => b,
                               addorsub => addorsub,
                               cin      => cin,
                               s        => s,
                               co       => co );

  stimulus: process
  begin
    -- Put initialisation code here
    a <= "10101011";
    b <= "01011001";
    cin <= '1';
    addorsub <= '1';
    
    wait for 10 ns;
    
    a <= "10100011";
    b <= "01000001";
    cin <= '1';
    addorsub <= '1';
    
    wait for 10 ns;
    
    a <= "10100011";
    b <= "10001111";
    cin <= '1';
    addorsub <= '1';
    
    wait for 10 ns;
    
    a <= "10101011";
    b <= "01011001";
    cin <= '1';
    addorsub <= '1';
    
    wait for 10 ns;
    
    a <= "10000111";
    b <= "11110001";
    cin <= '0';
    addorsub <= '0';
    
    wait for 10 ns;
    
    a <= "10101011";
    b <= "01010001";
    cin <= '1';
    addorsub <= '0';
    
    wait for 10 ns;
    
    a <= "10100011";
    b <= "01000001";
    cin <= '0';
    addorsub <= '0';
    
    wait for 10 ns;
    
    a <= "10100011";
    b <= "10001111";
    cin <= '1';
    addorsub <= '0';
    
    wait for 10 ns;
    
    a <= "10001011";
    b <= "01011001";
    cin <= '0';
    addorsub <= '0';
    
    wait for 10 ns;
    
    
    
    


    -- Put test bench stimulus code here
  end process;


end;