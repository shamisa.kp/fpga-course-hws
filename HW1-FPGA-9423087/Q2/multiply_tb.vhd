
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity multiply_tb is
end;

architecture bench of multiply_tb is

  component multiply
      port(a, b: in std_logic_vector(3 downto 0);
           result: out std_logic_vector(7 downto 0));
  end component;

  signal a, b: std_logic_vector(3 downto 0);
  signal result: std_logic_vector(7 downto 0);

begin

  uut: multiply port map ( a      => a,
                           b      => b,
                           result => result );

  stimulus: process
  begin
    
    -- Put initialisation code here
    a <= "1111";
    b <= "1111";
    wait for 100 ns;
    
    a <= "0100";
    b <= "0111";
    wait for 100 ns;
    
    a <= "0001";
    b <= "1011";
    wait for 100 ns;
    
    a <= "1000";
    b <= "0011";
    wait for 100 ns;
    
    a <= "1010";
    b <= "0001";
    wait for 10 ns;

  end process;


end;