library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity byteAddorSub is
    port(a : in std_logic_vector(7 downto 0);
         b : in std_logic_vector(7 downto 0);
         addorsub: in std_logic;
         cin : in std_logic;
         s : out std_logic_vector(7 downto 0);
         co : out std_logic);
end byteAddorSub;

architecture Behavioral of byteAddorSub is
    component fullAdder is 
            port (a, b, cin, addorSub: in std_logic;
                  sumorSub, cout: out std_logic);
    end component;
    signal c: std_logic_vector(8 downto 0);
begin
     
     gen: for i in 0 to 7 generate
             uut: fullAdder port map (a => a(i), b => b(i), cin => c(i),
                  sumorSub => s(i), cout => c(i+1), addorSub => addorsub);
     end generate;
     c(0) <= cin;
     co <= c(8);
     
end Behavioral;
