library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity divide is
    Port (a : in std_logic_vector(7 downto 0);
          b : in std_logic_vector(3 downto 0);
          r : out std_logic_vector(3 downto 0);
          quotient: out std_logic_vector(7 downto 0));
end divide;

architecture Behavioral of divide is
-- using byteAddorSub for subtracting
component byteAddorSub is 
        port(a : in std_logic_vector(7 downto 0);
             b : in std_logic_vector(7 downto 0);
             addorsub: in std_logic;
             cin : in std_logic;
             s : out std_logic_vector(7 downto 0);
             co : out std_logic);
         end component;

-- signals for storing b and a and r of each step
    signal b7, b6, b5, b4, b3, b2, b1, b0 : std_logic;
    signal a_0, a_1, a_2, a_3, a_4, a_5, a_6, a_7: std_logic_vector(7 downto 0);
    signal r_0, r_1, r_2, r_3, r_4, r_5, r_6, r_7: std_logic_vector(7 downto 0);
    signal b_0, b_1, b_2, b_3, b_4, b_5, b_6, b_7: std_logic_vector(7 downto 0);
    
begin
    a_0 <= a;
    b_0(7) <= b(0);
    b_0(6 downto 0) <= "0000000";
    
    firstStep: byteAddorSub port map(a => a_0, b => b_0, addorsub => '1', 
                                     cin => '0', co => b0, s => r_0);
    
    a_1 <= r_0 when b(3 downto 1) = "000" and b0 = '0' else a_0;
    quotient(7) <= '1' when b(3 downto 1) = "000" and b0 = '0' else '0';
    b_1(7 downto 6) <= b(1 downto 0);
    b_1(5 downto 0) <= "000000";
    
    secondStep: byteAddorSub port map(a => a_1, b => b_1, addorsub => '1', 
                                      cin => '0', co => b1, s => r_1);
    
    a_2 <= r_1 when b(3 downto 2) = "00" and b1 = '0' else a_1;
    quotient(6) <= '1' when b(3 downto 2) = "00" and b1 = '0' else '0';
    b_2(7 downto 5) <= b(2 downto 0);
    b_2(4 downto 0) <= "00000";
    
    thirdStep: byteAddorSub port map(a => a_2, b => b_2, addorsub => '1',
                                     cin => '0', co => b2, s => r_2);
    
    a_3 <= r_2 when b(3) = '0' and b2 = '0' else a_2;
    quotient(5) <= '1' when b(3) = '0' and b2 = '0' else '0';
    b_3(7 downto 4) <= b(3 downto 0);
    b_3(3 downto 0) <= "0000";
    
    fourthStep: byteAddorSub port map(a => a_3, b => b_3, addorsub => '1',
                                     cin => '0', co => b3, s => r_3);
    
    a_4 <= r_3 when b3 = '0' else a_3;
    quotient(4) <= '1' when b3 = '0' else '0';
    b_4(7) <= '0';
    b_4(6 downto 3) <= b(3 downto 0);
    b_4(2 downto 0) <= "000";
    
    fifthStep: byteAddorSub port map(a => a_4, b => b_4, addorsub => '1',
                                     cin => '0', co => b4, s => r_4);
                                     
    a_5 <= r_4 when b4 = '0' else a_4;
    quotient(3) <= '1' when b4 = '0' else '0';
    b_5(7 downto 6) <= "00";
    b_5(5 downto 2) <= b(3 downto 0);
    b_5(1 downto 0) <= "00";           
                               
    sixthStep: byteAddorSub port map(a => a_5, b => b_5, addorsub => '1',
                                     cin => '0', co => b5, s => r_5);
    
    a_6 <= r_5 when b5 = '0' else a_5;
    quotient(2) <= '1' when b5 = '0' else '0';
    b_6(7 downto 5) <= "000";
    b_6(4 downto 1) <= b(3 downto 0);
    b_6(0) <= '0'; 
    
    seventhStep: byteAddorSub port map(a => a_6, b => b_6, addorsub => '1',
                                       cin => '0', co => b6, s => r_6);
    
    a_7 <= r_6 when b6 = '0' else a_6;
    quotient(1) <= '1' when b6 = '0' else '0';
    b_7(7 downto 4) <= "0000";
    b_7(3 downto 0) <= b(3 downto 0);
    
    eighthStep: byteAddorSub port map(a => a_7, b => b_7, addorsub => '1',
                                           cin => '0', co => b7, s => r_7);
    r <= r_7(3 downto 0) when b7 = '0' else a_7(3 downto 0);
    quotient(0) <= '1' when b7 = '0' else '0';
    
    
end Behavioral;
