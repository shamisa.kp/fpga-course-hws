----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/20/2018 05:43:47 PM
-- Design Name: 
-- Module Name: MainQ1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fullAdder is
Port (a: in std_logic;
      b: in std_logic;
      addorSub: in std_logic; 
      cin: in std_logic;
      sumorSub: out std_logic;
      cout: out std_logic);
end fullAdder;

architecture gate of fullAdder is
signal co: std_logic;
signal bo:  std_logic;

begin
co <= ((a and b) or (cin and a) or (cin and b)) and (not addorSub);
bo <= ((cin and (not(a xor b))) or ((not a) and b)) and addorSub;
sumorSub <= a xor b xor cin;
cout <= co or bo;
 
end gate;
