library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity divide_tb is
end;

architecture bench of divide_tb is

  component divide
      Port (a : in std_logic_vector(7 downto 0);
            b : in std_logic_vector(3 downto 0);
            r : out std_logic_vector(3 downto 0);
            quotient: out std_logic_vector(7 downto 0));
  end component;

  signal a: std_logic_vector(7 downto 0);
  signal b: std_logic_vector(3 downto 0);
  signal r: std_logic_vector(3 downto 0);
  signal quotient: std_logic_vector(7 downto 0);

begin

  uut: divide port map ( a        => a,
                         b        => b,
                         r        => r,
                         quotient => quotient );

  stimulus: process
  begin
  
    -- Put initialisation code here
a <= "10111010";
b <= "1000";

wait for 100 ns;

a <= "11111111";
b <= "0001";

wait for 100 ns;

a <= "01101110";
b <= "1101";

wait for 100 ns;

a <= "00111001";
b <= "0111";

wait for 10 ns;

    -- Put test bench stimulus code here

    wait;
  end process;


end;